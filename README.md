SafeInsure Core integration/staging repository
=====================================

Safeinsure is a decentralized insurance marketplace bringing equitable price discovery and global access to insurance policy shoppers worldwide. SafeInsure puts consumer power back in your hands with an honest, accountable, and democratic insurance marketplace on the blockchain.
- Decentralized blockchain voting providing for consensus based advancement of the current Masternode technology used to secure the network and provide the above features, each Masternode is secured with collateral of 1000 SINS
- Anonymized transactions using coin mixing technology, we call it _Privatesend_.
- Fast transactions featuring guaranteed zero confirmation transactions, we call it _InstantSend_.

More information at [safeinsure.io](https://www.safeinsure.io)

### Coin Specs
|   |   |
|---|---|
|Type|POS + Masternode|
|Algo|Quark|
|Block Time|60 Seconds|
|Maturity|100 Confirmations|
|Difficulty Retargeting|Every Block|
|Masternode Collateral|1,000 SINS|
|P2P port|39105|
|RPC port|39106|
|Max Coin Supply|21,000,000 SINS|
|Premine|5% *|


*View Coin Distribution on [safeinsure.io](https://www.safeinsure.io)

### Reward Distribution


|BLock no|Reward / block|Staking|Masternodes|
|---|---|---|---|
|1-21600|0.5 SINS|0.10 SINS (20%)|0.40 SINS (80%)|
|21601-43200|1 SINS|0.20 SINS (20%)|0.80 SINS (80%)|
|43201-64800|1.5 SINS|0.30 SINS (20%)|1.20 SINS (80%)|
|64801-87840|2 SINS|0.40 SINS (20%)|1.60 SINS (80%)|
|87841-106560|2.5 SINS|0.37 SINS (15%)|2.12 SINS (85%)|
|106561-129600|3 SINS|0.45 SINS (15%)|2.55 SINS (85%)|
|129601-151200|3.5 SINS|0.52 SINS (15%)|2.97 SINS (85%)|
|151201-172800|4 SINS|0.60 SINS (15%)|3.40 SINS (85%)|
|172801-194400|4.5 SINS|0.45 SINS (10%)|4.05 SINS (90%)|
|194401-216000|5 SINS|0.50 SINS (10%)|4.50 SINS (90%)|
|216001-237600|5.5 SINS|0.55 SINS (10%)|4.95 SINS (90%)|
|237601-259200|6 SINS|0.60 SINS (10%)|5.40 SINS (90%)|
|259201-280800|6.5 SINS|0.65 SINS (10%)|5.85 SINS (90%)|
|280801-302400|7 SINS|0.70 SINS (10%)|6.30 SINS (90%)|
|302401-324000|7.5 SINS|0.75 SINS (10%)|6.75 SINS (90%)|
|324001-345600|8 SINS|0.8 0SINS (10%)|7.20 SINS (90%)|
|345601-367200|8.5 SINS|0.85 SINS (10%)|7.65 SINS (90%)|
|367201-388800|9 SINS|0.90 SINS (10%)|8.10 SINS (90%)|
|388801-525600|5 SINS|1 SINS (20%)|4 SINS (80%)|
|525601-1,051,200|4 SINS|0.8 SINS (20%)|3.2 SINS (80%)|
|*After 2 years, each year, the reward will be reduced with 10%|